# WorkXP

#### 介绍
WorkXP是一款为2D绘图仪设计的上位机控制软件。

#### 运行环境
QT5.14

QT Creator 打开workxp.pro

#### 绘制矩形
![绘制矩形](https://images.gitee.com/uploads/images/2020/0629/214748_5e6eccd6_1789340.gif "workxp_1.gif")

#### 绘制曲线
![绘制曲线](https://images.gitee.com/uploads/images/2020/0629/214858_55d5c6e2_1789340.gif "workxp_2.gif")
